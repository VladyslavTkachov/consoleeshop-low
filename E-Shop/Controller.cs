﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E_Shop
{
    public static class Controller
    {

        public static void ChoiceInterface()
        {
            if (DB_Shop.CurrentUser is Admin)
                User_Intertface.AdminInterface();
            else if(DB_Shop.CurrentUser is User)
                User_Intertface.UserInterface();
            else
                User_Intertface.GuestInterface();
        }

        public static void Start()
        {
            User_Intertface.Hello();
            ChoiceInterface();
        }

        public static void ViewProductControler()
        {
            User_Intertface.ViewProductInterface();
            Performers.ViewProduct();
        }
        public static void SearchProductControler()
        {
            Performers.SearchProduct(User_Intertface.SearchProductInterface());
        }
        public static void RegisterController()
        {
            User_Intertface.RegisterInterface(out string name, out string password);
            Performers.CreateNewUser(name, password);
        }

        public static void LogInControler()
        {
            User_Intertface.LogInInterface(out string name, out string password);
            Performers.LogIn(name, password);
        }

        public static void CreateNewOrderControler()
        {
            Order order = new Order();
            User_Intertface.CreateOrderInterface(out List<int> id);
            foreach(var item in id)
                 Performers.AddProductToOrder(item, order);
            Performers.AddOrderToUser(order);
        }

        public static void RegisterOrderControler()
        {
            User_Intertface.StatusForOrderInterface();
        }

        public static void HistoryOrderControler()
        {
            User_Intertface.ViewHistoryOrderInterface();
            Performers.ViewHistoryOrders();
        }

        public static void StatusForOrderControler()
        {
            User_Intertface.StatusForOrderInterface();
        }

        public static void ChangeDataForUserControler()
        {
            User_Intertface.ChangeDataUserInterface(out string name, out string password);
            Performers.ChangeUserData(name, password);
        }

        public static void ChangeDataForAdminControler()
        {
            User_Intertface.ViewUsersInterface();
            Performers.ChangeDataUsers();
        }

        public static void ExitControler()
        {
            DB_Shop.CurrentUser = null;
            ChoiceInterface();
        }

        public static void AddProductControler()
        {
            User_Intertface.AddProductToBaseInterface(out int id, out string name, out string category, out string description, out double price);
            Performers.AddProductToBase(id, name, category, description, price);
        }

        public static void ChangeIformForProductControler()
        {
            User_Intertface.ChangeInformForItemInterface(out Product product);
            Performers.ChangeProduct(product);
        }

        public static void ChangeStatusOrderForAdminControler()
        {
            User_Intertface.ChangeStatusOrderForAdminInterface();
            Performers.ChangeStatusOrderForAdmin();
        }

    }
}
