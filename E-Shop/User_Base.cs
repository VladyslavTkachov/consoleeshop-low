﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E_Shop
{
    public abstract class User_Base
    {

        public List<Order> orders = new List<Order>();
        protected User_Base(string name, string password)
        {
            Name = name;
            Password = password;
        }
        public string Name
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public Order Add_Order
        {
            set => orders.Add(value);
        }
    }

    public class Admin: User_Base
    {
        public Admin(string name, string password): base(name,password)
        {

        }
        public string Status = "Admin";
        
        public List<Order> Return_Orders
        {
            get => orders;
        }
    }

    public class User : User_Base
    {
        public User(string name, string password) : base(name, password)
        {

        }
    }



}
