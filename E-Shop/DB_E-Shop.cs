﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E_Shop
{

    public static class DB_Shop
    {
        public static List<Product> Goods = new List<Product>()
        {
            new Product(1,"AMD Ryzen 2600","processor" ,"core/threads (6/12) boost:4.2 GHz",180),
            new Product(2,"AMD Ryzen 5600x","processor" ,"core/threads (6/12) boost:4.9 GHz",350),
            new Product(3,"AMD Ryzen 3200G","processor" ,"core/threads (4/4) boost:3.7 GHz",170),
            new Product(4,"AMD Ryzen 3400G","processor" ,"core/threads (4/8) boost:3.9 GHz",22.40),
            new Product(5,"Intel Core i5-10600K","processor" ,"core/threads (6/12) boost:4.8 GHz",18.35),
            new Product(6,"Intel Core i7-10700K","processor" ,"core/threads (8/16) boost:5.1 GHz",12.60),
            new Product(7,"Intel Core i5-10400f","processor" ,"core/threads (6/12) boost:4.4 GHz",7.20),
            new Product(8,"Nvidia RTX 3060","graphics card" ,"GDDR6: 12gb 192 bit,",350),
            new Product(9,"Nvidia RTX 3070","graphics card" ,"GDDR6: 8gb 256 bit",700),
            new Product(10,"Nvidia RTX 3090","graphics card" ,"GDDR6: 24gb 384 bit,",1500)
        };

        public static List<User_Base> Users = new List<User_Base>() { new Admin("admin", "admin") };

        public static User_Base CurrentUser = null;


    }

    public static class Switch
    {
        public delegate void del();
        public static Dictionary<int, Delegate> GuestDictionary = new Dictionary<int, Delegate>
        {
            {1,new del(Controller.ViewProductControler)},
            {2,new del(Controller.SearchProductControler)},
            {3,new del(Controller.RegisterController)},
            {4,new del(Controller.LogInControler)}
        };

        public static Dictionary<int, Delegate> UserDictionary = new Dictionary<int, Delegate>
        {
            {1,new del(Controller.ViewProductControler)},
            {2,new del(Controller.SearchProductControler)},
            {3,new del(Controller.CreateNewOrderControler)},
            {4,new del(Controller.RegisterOrderControler)},
            {5,new del(Controller.HistoryOrderControler)},
            {6,new del(Controller.StatusForOrderControler)},
            {7,new del(Controller.ChangeDataForUserControler)},
            {8,new del(Controller.ExitControler)}
        };

        public static Dictionary<int, Delegate> AdminDictionary = new Dictionary<int, Delegate>
        {
            {1,new del(Controller.ViewProductControler)},
            {2,new del(Controller.SearchProductControler)},
            {3,new del(Controller.CreateNewOrderControler)},
            {4,new del(Controller.RegisterOrderControler)},
            {5,new del(Controller.ChangeDataForAdminControler)},
            {6,new del(Controller.AddProductControler)},
            {7,new del(Controller.ChangeIformForProductControler)},
            {8,new del(Controller.ChangeStatusOrderForAdminControler)},
            {9,new del(Controller.ExitControler)}
        };
    }
}
