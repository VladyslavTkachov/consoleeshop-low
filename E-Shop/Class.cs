﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E_Shop
{



    public class Product
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public double Price
        {
            get;
            set;
        }

        public string Category
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public Product(int id, string name, string category, string description, double price)
        {
            Id = id;
            Name = name;
            Price = price;
            Category = category;
            Description = description;
        }
    }

    public class Order
    {
        double sum;
        public List<Product> products = new List<Product>();

        public string Status
        {
            get;
            set;
        }

        public double Sum
        {
            get
            {
                sum = 0;
                foreach (var item in products)
                    sum += item.Price;
                return sum;
            }
        }

        public Product AddProduct
        {
            set => products.Add(value);
        }
    }
}
