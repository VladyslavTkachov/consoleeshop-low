﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E_Shop
{
    public static class Performers
    {
        public static void AddOrderToUser(Order order)
        {
            order.Status = "New";
            DB_Shop.CurrentUser.Add_Order = order;
        }

        public static void AddProductToOrder(int id, Order order)
        {
            foreach(var item in DB_Shop.Goods)
            {
                if (id == item.Id)
                    order.AddProduct = item;
            }
        }

        public static void AddProductToBase(int id,string name,string category,string description,double price)
        {
            Product product = new Product(id,name, category, description, price);
            DB_Shop.Goods.Add(product);
            Console.WriteLine("Успешно добавлен новый товар. Нажмите любую клавишу");
            Console.ReadKey();
            Controller.ChoiceInterface();
        }

        public static void CreateNewUser(string name, string password)
        {
            User user = new User(name, password);
            foreach(var item in DB_Shop.Users)
            {
                if (item.Name == name)
                {
                    Console.WriteLine("Пользователь с таким именем уже существует!");
                    return;
                }
            }
            DB_Shop.Users.Add(user);
            Console.WriteLine("Пользователь успешно создан");
            Console.WriteLine("Нажмите любую клавишу и авторизируйтесь");
            Console.ReadKey();
            Controller.ChoiceInterface();
        }

        public static void SearchProduct(string name)
        {
            foreach(var item in DB_Shop.Goods)
            {
                if (item.Name == name)
                {
                    Console.WriteLine("Товар с таким именем найден");
                    Console.WriteLine($"{item.Id,-5} |{item.Name,-20} |{item.Category,-20} |{item.Description,-50} |{item.Price,-5}");
                    Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
                    Console.ReadKey();
                    Controller.ChoiceInterface();
                }    
            }
            Console.WriteLine("Товар с таким названием не найден");
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            Controller.ChoiceInterface();
        }

        public static void LogIn(string name, string password)
        {
            foreach(var item in DB_Shop.Users)
            {
                if (item.Name == name)
                {
                    if (item.Password == password)
                    {
                        DB_Shop.CurrentUser = item;
                        Console.WriteLine($"Добро пожаловать {DB_Shop.CurrentUser.Name} !");
                        Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
                        Console.ReadKey();
                        Controller.ChoiceInterface();
                    }
                    else
                    {
                        Console.WriteLine("Неверно введен пароль");
                        Console.ReadKey();
                        Controller.ChoiceInterface();
                    }
                    
                }
            }
            Console.ReadKey();
            Controller.ChoiceInterface();
        }

        public static void ViewHistoryOrders()
        {
            foreach(var item in DB_Shop.CurrentUser.orders)
            {
                Console.WriteLine($"Статус заказа :{item.Status}");
                Console.WriteLine($"Список товаров :");
                foreach(var prod in item.products)
                    Console.WriteLine($"{prod.Id,-5} |{prod.Name,-20} |{prod.Category,-20} |{prod.Description,-50} |{prod.Price,-5}");
                Console.WriteLine($"Сумма заказа :{item.Sum}");
                Console.WriteLine($"\n\n");
            }
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            Controller.ChoiceInterface();
        }

        public static void ViewProduct()
        {
            foreach(var item in DB_Shop.Goods)
            {
                Console.WriteLine($"{item.Id,-5} |{item.Name,-20} |{item.Category,-20} |{item.Description,-50} |{item.Price,-5}");
            }
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            Controller.ChoiceInterface();
        }

        public static void ChangeUserData(string name, string password)
        {
            DB_Shop.CurrentUser.Name = name;
            DB_Shop.CurrentUser.Password = password;
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            Controller.ChoiceInterface();
        }

        public static void ChangeProduct(Product product)
        {
            if(product == null)
            {
                Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
                Console.ReadKey();
                Controller.ChoiceInterface();
            }
            foreach(var item in DB_Shop.Goods)
            {
                if(item.Id == product.Id)
                {
                    item.Name = product.Name;
                    item.Category = product.Category;
                    item.Description = product.Description;
                    item.Price = product.Price;
                    Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
                    Console.ReadKey();
                    Controller.ChoiceInterface();
                }
            }
        }

        public static void ChangeDataUsers()
        {
            Console.WriteLine("Введите номер пользователя");
            int number = Convert.ToInt32(Console.ReadLine()) - 1;
            Console.WriteLine("Введите новое имя пользователя");
            DB_Shop.Users[number].Name = Console.ReadLine();
            Console.WriteLine("Введите новый пароль пользователя");
            DB_Shop.Users[number].Password = Console.ReadLine();
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            Controller.ChoiceInterface();
        }

        public static void ChangeStatusOrderForAdmin()
        {
            Console.WriteLine("Введите номер пользователя");
            int number = Convert.ToInt32(Console.ReadLine()) - 1;
            Console.WriteLine("Введите номер заказа");
            int number2 = Convert.ToInt32(Console.ReadLine()) - 1;
            Console.WriteLine("Выберете статус:");
            Console.WriteLine("1 - Отмена заказа администратором");
            Console.WriteLine("2 - Получена оплата");
            Console.WriteLine("3 - Отправлено");
            Console.WriteLine("4 - Получено");
            Console.WriteLine("5 - Завершено");
            int command = Convert.ToInt32(Console.ReadLine());
            if(command == 1)
                DB_Shop.Users[number].orders[number2].Status = "Отмена заказа администратором";
            if (command == 2)
                DB_Shop.Users[number].orders[number2].Status = "Получена оплата";
            if (command == 3)
                DB_Shop.Users[number].orders[number2].Status = "Отправлен";
            if (command == 4)
                DB_Shop.Users[number].orders[number2].Status = "Получено";
            if (command == 5)
                DB_Shop.Users[number].orders[number2].Status = "Завершено";

            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            Controller.ChoiceInterface();
        }
    }
}
