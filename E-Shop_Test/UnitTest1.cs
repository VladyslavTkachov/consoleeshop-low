using NUnit.Framework;

namespace E_Shop
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void AddOrderToUseTest()
        {
            DB_Shop.CurrentUser = new User("User", "user");
            Order order = new Order();
            order.AddProduct = new Product(1, "AMD Ryzen 2600", "processor", "core/threads (6/12) boost:4.2 GHz", 180);

            Performers.AddOrderToUser(order);

            Assert.AreEqual(DB_Shop.CurrentUser.orders[0], order, "Method is incorrectly");
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        public void AddProductToOrderTest(int id)
        {
            Order order = new Order();

            Performers.AddProductToOrder(id, order);

            Assert.AreEqual(order.products[0], DB_Shop.Goods[id - 1], "Method is incorrectly");
        }

       
    }
}